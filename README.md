french documentation : [french documentation](https://gitlab.com/gilou_/windlogger_shield/wikis/Home_FR)

# widlogger_shield

This is the branch for the dev version : v2.1.1 clitch correction from v2.1.1

This board contains the functions to convert the signals from sensors to signals for the microcontroller.

This board is design to be plug in the digital board, but you can use it with an arduino uno or mega.

Comment are insert in the schematic, open the kicad project or the pdf export to read those.

## BLOCK DIAGRAM
![windlogger shield block diagram](/Hardware/shield/shield_bd.png)

## RENDER PREVIEW
![windlogger shield render preview](/Hardware/shield/shield_pcb.png)

## Todo list
- chose the componants
- update the BOM
- route the board
- test
