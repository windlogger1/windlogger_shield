EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Windlogger Shield"
Date "2020-02-21"
Rev "2.1.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPLv3"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 2125 5625 2    60   ~ 0
Anemometer1 : Hall effect or ILS
Text Notes 1925 4775 2    60   ~ 0
Winevane : potentiometer
Text Notes 1900 950  2    60   ~ 0
9V transformer : AC voltage
$Sheet
S 3525 5400 1400 850 
U 560A35D3
F0 "F1 Anemometers" 50
F1 "F1_anemometers.sch" 50
F2 "Wind1" I L 3525 5600 60 
F3 "Wind2" I L 3525 6100 60 
F4 "Speed2" O R 4925 6050 60 
F5 "Speed1" O R 4925 5600 60 
$EndSheet
$Sheet
S 3525 4525 1400 450 
U 560A35DE
F0 "F2 windvane" 50
F1 "F2_windvane.sch" 50
F2 "Udir" O R 4925 4775 60 
F3 "vane_v+" I L 3525 4650 60 
F4 "vane_sig" I L 3525 4750 60 
F5 "vane_v-" I L 3525 4850 60 
$EndSheet
$Sheet
S 3525 800  1400 900 
U 560A361A
F0 "F3_Voltage_sensors" 50
F1 "F3_Voltage_sensors.sch" 50
F2 "UAC_hi" I L 3525 900 60 
F3 "UAC_lo" I L 3525 1050 60 
F4 "Uac" O R 4925 950 60 
F5 "UDC" O R 4925 1500 60 
F6 "Ubat" I L 3525 1500 60 
$EndSheet
$Sheet
S 3525 2300 1400 1450
U 560A3643
F0 "F4_Current_sensors" 50
F1 "F4_Current_sensors.sch" 50
F2 "I1_hi" I L 3525 2450 60 
F3 "I1_lo" I L 3525 2550 60 
F4 "I1" O R 4925 2500 60 
F5 "I2_hi" I L 3525 2800 60 
F6 "I2_lo" I L 3525 2900 60 
F7 "I2" O R 4925 2850 60 
F8 "I3_hi" I L 3525 3150 60 
F9 "I3_lo" I L 3525 3250 60 
F10 "I3" O R 4925 3200 60 
F11 "I4_hi" I L 3525 3500 60 
F12 "I4_lo" I L 3525 3600 60 
F13 "I4" O R 4925 3550 60 
$EndSheet
Text Label 2925 5600 0    60   ~ 0
Wind1
Text Label 2975 6100 0    60   ~ 0
Wind2
Text Label 2875 4750 0    60   ~ 0
vane_sig
Text Label 3025 900  0    60   ~ 0
UAC_hi
Text Label 3025 1050 0    60   ~ 0
UAC_lo
Text Notes 10125 4625 0    60   ~ 0
CLKO/ICP1
Text Notes 10125 4525 0    60   ~ 0
OC1A/PWM
Text Notes 10125 4425 0    60   ~ 0
OC1B/SS/PWM
Text Notes 10125 4325 0    60   ~ 0
OC2A/MOSI/PWM
Text Notes 10125 4225 0    60   ~ 0
MISO
Text Notes 10125 4125 0    60   ~ 0
SCK
Text Notes 10125 4025 0    60   ~ 0
GND
Text Notes 10125 3925 0    60   ~ 0
AREF
Text Notes 10125 5500 0    60   ~ 0
RXD
Text Notes 10125 5400 0    60   ~ 0
TXD
Text Notes 10125 5300 0    60   ~ 0
INT0
Text Notes 10125 5200 0    60   ~ 0
INT1/OC2B
Text Notes 10125 5100 0    60   ~ 0
XCK/T0
Text Notes 10125 5000 0    60   ~ 0
OC0B/T1
Text Notes 10125 4900 0    60   ~ 0
OC0A/AIN0
Text Notes 8625 4350 0    60   ~ 0
GND
Text Notes 8625 4150 0    60   ~ 0
3.3V
Text Notes 8625 4250 0    60   ~ 0
5V
Text Notes 8625 4050 0    60   ~ 0
RESET
Text Notes 10125 4800 0    60   ~ 0
AIN1
Text Label 5350 5600 2    60   ~ 0
Speed1
Text Label 5350 6050 2    60   ~ 0
Speed2
Text Label 9325 5050 0    60   ~ 0
Speed1
Text Label 9325 4950 0    60   ~ 0
Speed2
Text Label 7250 3850 0    60   ~ 0
5V
NoConn ~ 8275 4500
Text Notes 10125 3825 0    60   ~ 0
SDA
Text Notes 10125 3725 0    60   ~ 0
SCL
NoConn ~ 9775 3700
NoConn ~ 9775 3800
NoConn ~ 9775 3900
NoConn ~ 9775 4500
NoConn ~ 9775 4600
NoConn ~ 9775 5350
NoConn ~ 9775 5450
Text Notes 8625 3950 0    60   ~ 0
IOREF
Text Notes 8625 4450 0    60   ~ 0
GND
Text Notes 8625 4550 0    60   ~ 0
Vin
Text Notes 8625 3850 0    60   ~ 0
Reserved
Text Notes 8425 3450 0    60   ~ 0
Arduino style but with Atmega 1284P
Text Notes 1975 6900 2    60   ~ 0
Temperature DS18B20
Text Label 2875 4850 0    60   ~ 0
vane_v-
Text Label 2875 4650 0    60   ~ 0
vane_v+
NoConn ~ 8275 3800
NoConn ~ 8275 3900
NoConn ~ 8275 4000
Text Notes 2100 6125 2    60   ~ 0
Anemometer2 : Hall effect or ILS
$Comp
L shield-rescue:+5V-power1 #PWR013
U 1 1 573C3921
P 7425 3725
F 0 "#PWR013" H 7425 3575 50  0001 C CNN
F 1 "+5V" H 7425 3865 50  0000 C CNN
F 2 "" H 7425 3725 50  0000 C CNN
F 3 "" H 7425 3725 50  0000 C CNN
	1    7425 3725
	1    0    0    -1  
$EndComp
NoConn ~ 8275 4100
NoConn ~ 9775 4850
$Comp
L shield-rescue:C-shield-rescue C1
U 1 1 57E733BC
P 7675 4150
F 0 "C1" H 7700 4250 50  0000 L CNN
F 1 "100nF" H 7700 4050 50  0000 L CNN
F 2 "windlog:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 7713 4000 50  0001 C CNN
F 3 "" H 7675 4150 50  0000 C CNN
	1    7675 4150
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CP-shield-rescue C2
U 1 1 57E734D2
P 7925 4150
F 0 "C2" H 7950 4250 50  0000 L CNN
F 1 "10uF" H 7950 4050 50  0000 L CNN
F 2 "windlog:CP_Radial_D5.0mm_P2.50mm" H 7963 4000 50  0001 C CNN
F 3 "" H 7925 4150 50  0000 C CNN
	1    7925 4150
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:PWR_FLAG-power1 #FLG01
U 1 1 57E743BD
P 7175 3750
F 0 "#FLG01" H 7175 3845 50  0001 C CNN
F 1 "PWR_FLAG" H 7175 3930 50  0000 C CNN
F 2 "" H 7175 3750 50  0000 C CNN
F 3 "" H 7175 3750 50  0000 C CNN
	1    7175 3750
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:PWR_FLAG-power1 #FLG02
U 1 1 57E7446B
P 7175 4325
F 0 "#FLG02" H 7175 4420 50  0001 C CNN
F 1 "PWR_FLAG" H 7175 4505 50  0000 C CNN
F 2 "" H 7175 4325 50  0000 C CNN
F 3 "" H 7175 4325 50  0000 C CNN
	1    7175 4325
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR04
U 1 1 57E7ADEC
P 2625 2100
F 0 "#PWR04" H 2625 1950 50  0001 C CNN
F 1 "+5V" H 2625 2240 50  0000 C CNN
F 2 "" H 2625 2100 50  0000 C CNN
F 3 "" H 2625 2100 50  0000 C CNN
	1    2625 2100
	1    0    0    -1  
$EndComp
Text Notes 600  3325 0    55   ~ 0
The 4 current sensors are design \nto are :\nHTFS 200 or Hass 50-s \nfor DC current\nSCT-013-000 \nfor AC current
Text Notes 2150 1400 2    60   ~ 0
Ubat
Text Label 3250 1500 2    60   ~ 0
Ubat
Text Label 9425 2375 0    60   ~ 0
Ubat
Text Label 9425 2575 0    60   ~ 0
UAC_hi
Text Label 9425 2675 0    60   ~ 0
UAC_lo
Text Notes 8850 3000 0    60   ~ 0
To feed power supply from voltage sensor.\nThe power supply block is on the digital board.
Text Notes 1975 1600 0    60   ~ 0
gnd
Wire Wire Line
	2525 5600 3525 5600
Wire Wire Line
	2525 6100 3525 6100
Wire Wire Line
	4925 5600 5350 5600
Wire Wire Line
	4925 6050 5350 6050
Wire Wire Line
	9775 4950 9325 4950
Wire Wire Line
	9775 5050 9325 5050
Wire Wire Line
	8200 4300 8200 4400
Wire Wire Line
	8200 4300 8275 4300
Wire Wire Line
	2525 4750 3525 4750
Wire Wire Line
	2525 4650 3525 4650
Wire Wire Line
	2525 4850 3525 4850
Wire Wire Line
	2525 5700 2700 5700
Wire Wire Line
	2700 5700 2700 5750
Wire Wire Line
	2525 6200 2700 6200
Wire Wire Line
	2700 6200 2700 6250
Wire Wire Line
	8200 4400 8275 4400
Connection ~ 8200 4400
Wire Wire Line
	2525 2250 2625 2250
Wire Wire Line
	2625 2100 2625 2250
Wire Wire Line
	2625 2800 2525 2800
Connection ~ 2625 2250
Wire Wire Line
	2625 3350 2525 3350
Connection ~ 2625 2800
Wire Wire Line
	2625 3850 2525 3850
Connection ~ 2625 3350
Wire Wire Line
	2525 2350 2725 2350
Wire Wire Line
	2725 2350 2725 2900
Wire Wire Line
	2525 2900 2725 2900
Connection ~ 2725 2900
Wire Wire Line
	2525 3450 2725 3450
Connection ~ 2725 3450
Wire Wire Line
	2525 3950 2725 3950
Connection ~ 2725 3950
Wire Wire Line
	3150 3550 2525 3550
Wire Wire Line
	3250 3650 2525 3650
Wire Wire Line
	2525 3000 3000 3000
Wire Wire Line
	3325 4050 2525 4050
Wire Wire Line
	2525 4150 3400 4150
Wire Wire Line
	9425 2375 9775 2375
Wire Wire Line
	9425 2675 9775 2675
Wire Wire Line
	2975 1725 2975 1600
NoConn ~ 9775 5700
NoConn ~ 9775 5800
NoConn ~ 9775 6100
NoConn ~ 9775 6200
NoConn ~ 9775 6300
NoConn ~ 9775 6400
Text Label 7200 4400 0    60   ~ 0
GND
$Comp
L shield-rescue:GND-power1 #PWR014
U 1 1 58041636
P 7425 4525
F 0 "#PWR014" H 7425 4275 50  0001 C CNN
F 1 "GND" H 7425 4375 50  0000 C CNN
F 2 "" H 7425 4525 50  0000 C CNN
F 3 "" H 7425 4525 50  0000 C CNN
	1    7425 4525
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR016
U 1 1 58041ACE
P 9250 4000
F 0 "#PWR016" H 9250 3750 50  0001 C CNN
F 1 "GND" H 9250 3850 50  0000 C CNN
F 2 "" H 9250 4000 50  0000 C CNN
F 3 "" H 9250 4000 50  0000 C CNN
	1    9250 4000
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR07
U 1 1 58043951
P 2725 4250
F 0 "#PWR07" H 2725 4000 50  0001 C CNN
F 1 "GND" H 2725 4100 50  0000 C CNN
F 2 "" H 2725 4250 50  0000 C CNN
F 3 "" H 2725 4250 50  0000 C CNN
	1    2725 4250
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR08
U 1 1 58044479
P 2975 1725
F 0 "#PWR08" H 2975 1475 50  0001 C CNN
F 1 "GND" H 2975 1575 50  0000 C CNN
F 2 "" H 2975 1725 50  0000 C CNN
F 3 "" H 2975 1725 50  0000 C CNN
	1    2975 1725
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR05
U 1 1 58044D95
P 2700 5750
F 0 "#PWR05" H 2700 5500 50  0001 C CNN
F 1 "GND" H 2700 5600 50  0000 C CNN
F 2 "" H 2700 5750 50  0000 C CNN
F 3 "" H 2700 5750 50  0000 C CNN
	1    2700 5750
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR06
U 1 1 58044FD5
P 2700 6250
F 0 "#PWR06" H 2700 6000 50  0001 C CNN
F 1 "GND" H 2700 6100 50  0000 C CNN
F 2 "" H 2700 6250 50  0000 C CNN
F 3 "" H 2700 6250 50  0000 C CNN
	1    2700 6250
	1    0    0    -1  
$EndComp
NoConn ~ 7350 6075
NoConn ~ 7350 6200
NoConn ~ 8325 6075
NoConn ~ 8325 6200
Text Notes 8125 5875 0    60   ~ 0
Fixation Holes
NoConn ~ 9775 5150
$Comp
L shield-rescue:GND-power1 #PWR03
U 1 1 5852ED8A
P 2575 7050
F 0 "#PWR03" H 2575 6800 50  0001 C CNN
F 1 "GND" H 2575 6900 50  0000 C CNN
F 2 "" H 2575 7050 50  0000 C CNN
F 3 "" H 2575 7050 50  0000 C CNN
	1    2575 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2575 7050 2575 6975
Wire Wire Line
	2575 6975 2500 6975
Wire Wire Line
	2500 6875 3200 6875
Wire Wire Line
	3175 6775 2500 6775
Wire Wire Line
	9300 5900 9775 5900
Wire Wire Line
	9300 6000 9775 6000
Text Label 3200 6875 2    60   ~ 0
1wire_data
Text Label 9300 6000 0    60   ~ 0
1wire_PWR
Text Label 9300 5900 0    60   ~ 0
1wire_data
Text Label 3175 6775 2    60   ~ 0
1wire_PWR
Wire Notes Line
	2075 2175 2075 4325
Wire Notes Line
	2075 4325 2200 4325
Wire Notes Line
	2200 2175 2075 2175
NoConn ~ 9775 4400
$Comp
L shield-rescue:CONN_01X03-shield-rescue P6
U 1 1 5855584D
P 2325 4750
F 0 "P6" H 2225 4950 50  0000 C CNN
F 1 "vane" V 2425 4750 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 2325 4750 50  0001 C CNN
F 3 "" H 2325 4750 50  0000 C CNN
	1    2325 4750
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X02-shield-rescue P9
U 1 1 58556D71
P 2375 1550
F 0 "P9" H 2300 1700 50  0000 C CNN
F 1 "Udc" V 2475 1625 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_2-G-5,08_1x02_P5.08mm_Horizontal" H 2375 1550 50  0001 C CNN
F 3 "" H 2375 1550 50  0000 C CNN
	1    2375 1550
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X04-shield-rescue P2
U 1 1 5855938E
P 2325 2400
F 0 "P2" H 2225 2650 50  0000 C CNN
F 1 "current1" V 2525 2425 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 2325 2400 50  0001 C CNN
F 3 "" H 2325 2400 50  0000 C CNN
	1    2325 2400
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X04-shield-rescue P3
U 1 1 5855A2A9
P 2325 2950
F 0 "P3" H 2225 3200 50  0000 C CNN
F 1 "current2" V 2525 2950 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 2325 2950 50  0001 C CNN
F 3 "" H 2325 2950 50  0000 C CNN
	1    2325 2950
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X04-shield-rescue P4
U 1 1 5855A3B5
P 2325 3500
F 0 "P4" H 2225 3750 50  0000 C CNN
F 1 "current3" V 2525 3475 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 2325 3500 50  0001 C CNN
F 3 "" H 2325 3500 50  0000 C CNN
	1    2325 3500
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X04-shield-rescue P5
U 1 1 5855AFF4
P 2325 4000
F 0 "P5" H 2225 4250 50  0000 C CNN
F 1 "current4" V 2525 4025 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_4-G-5,08_1x04_P5.08mm_Horizontal" H 2325 4000 50  0001 C CNN
F 3 "" H 2325 4000 50  0000 C CNN
	1    2325 4000
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X03-shield-rescue P1
U 1 1 5855F710
P 2300 6875
F 0 "P1" H 2200 7075 50  0000 C CNN
F 1 "temp" V 2400 6875 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 2300 6875 50  0001 C CNN
F 3 "" H 2300 6875 50  0000 C CNN
	1    2300 6875
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X01-shield-rescue h1
U 1 1 585640DC
P 7550 6075
F 0 "h1" H 7250 6075 50  0000 C CNN
F 1 "Mounting hole" H 7850 6075 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 7550 6075 50  0001 C CNN
F 3 "" H 7550 6075 50  0000 C CNN
	1    7550 6075
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X01-shield-rescue h2
U 1 1 58564977
P 7550 6200
F 0 "h2" H 7250 6200 50  0000 C CNN
F 1 "Mounting hole" H 7850 6200 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 7550 6200 50  0001 C CNN
F 3 "" H 7550 6200 50  0000 C CNN
	1    7550 6200
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X01-shield-rescue h3
U 1 1 58564B74
P 8525 6075
F 0 "h3" H 8225 6075 50  0000 C CNN
F 1 "Mounting hole" H 8825 6075 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 8525 6075 50  0001 C CNN
F 3 "" H 8525 6075 50  0000 C CNN
	1    8525 6075
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X01-shield-rescue h4
U 1 1 58564CA1
P 8525 6200
F 0 "h4" H 8225 6200 50  0000 C CNN
F 1 "Mounting hole" H 8825 6200 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380" H 8525 6200 50  0001 C CNN
F 3 "" H 8525 6200 50  0000 C CNN
	1    8525 6200
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X08-shield-rescue P13
U 1 1 58566141
P 8475 5225
F 0 "P13" H 8475 5675 50  0000 C CNN
F 1 "Analog" V 8575 5225 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 8475 5225 50  0001 C CNN
F 3 "" H 8475 5225 50  0000 C CNN
	1    8475 5225
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X08-shield-rescue P12
U 1 1 585670D0
P 8475 4150
F 0 "P12" H 8475 4600 50  0000 C CNN
F 1 "Power" V 8575 4150 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 8475 4150 50  0001 C CNN
F 3 "" H 8475 4150 50  0000 C CNN
	1    8475 4150
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X10-shield-rescue P15
U 1 1 58567C59
P 9975 4150
F 0 "P15" H 9975 4700 50  0000 C CNN
F 1 "PWM1" V 10075 4150 50  0000 C CNN
F 2 "windlog:PinSocket_1x10_P2.54mm_Vertical" H 9975 4150 50  0001 C CNN
F 3 "" H 9975 4150 50  0000 C CNN
	1    9975 4150
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X08-shield-rescue P16
U 1 1 585689E5
P 9975 5100
F 0 "P16" H 10125 5500 50  0000 C CNN
F 1 "PWM2" V 10075 5100 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 9975 5100 50  0001 C CNN
F 3 "" H 9975 5100 50  0000 C CNN
	1    9975 5100
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X08-shield-rescue P17
U 1 1 58569A7E
P 9975 6050
F 0 "P17" H 9975 6500 50  0000 C CNN
F 1 "Communication" V 10075 6050 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 9975 6050 50  0001 C CNN
F 3 "" H 9975 6050 50  0000 C CNN
	1    9975 6050
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X04-shield-rescue P14
U 1 1 5857136B
P 9975 2525
F 0 "P14" H 9975 2775 50  0000 C CNN
F 1 "power_source" V 10125 2500 50  0000 C CNN
F 2 "windlog:PinHeader_1x04_P2.54mm_Vertical" H 9975 2525 50  0001 C CNN
F 3 "" H 9975 2525 50  0000 C CNN
	1    9975 2525
	1    0    0    -1  
$EndComp
Wire Notes Line
	6950 3300 6950 6750
$Comp
L shield-rescue:CONN_01X03-shield-rescue P11
U 1 1 585985B9
P 5825 7275
F 0 "P11" H 5750 7475 50  0000 C CNN
F 1 "Digi2" V 5925 7275 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 5825 7275 50  0001 C CNN
F 3 "" H 5825 7275 50  0000 C CNN
	1    5825 7275
	-1   0    0    1   
$EndComp
Wire Wire Line
	4925 1500 5400 1500
$Comp
L shield-rescue:BARREL_JACK-shield-rescue BJ1
U 1 1 585E60F9
P 2450 1000
F 0 "BJ1" H 2450 1250 50  0000 C CNN
F 1 "Uac" H 2450 800 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 2450 1000 50  0001 C CNN
F 3 "" H 2450 1000 50  0000 C CNN
	1    2450 1000
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:CONN_01X03-shield-rescue P7
U 1 1 5862AA9C
P 2325 5600
F 0 "P7" H 2225 5800 50  0000 C CNN
F 1 "anemo1" V 2425 5600 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 2325 5600 50  0001 C CNN
F 3 "" H 2325 5600 50  0000 C CNN
	1    2325 5600
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:CONN_01X03-shield-rescue P8
U 1 1 5862E3CC
P 2325 6100
F 0 "P8" H 2225 6300 50  0000 C CNN
F 1 "anemo2" V 2425 6100 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 2325 6100 50  0001 C CNN
F 3 "" H 2325 6100 50  0000 C CNN
	1    2325 6100
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR01
U 1 1 5862F4AB
P 2575 5500
F 0 "#PWR01" H 2575 5350 50  0001 C CNN
F 1 "+5V" H 2575 5640 50  0000 C CNN
F 2 "" H 2575 5500 50  0000 C CNN
F 3 "" H 2575 5500 50  0000 C CNN
	1    2575 5500
	0    1    1    0   
$EndComp
$Comp
L shield-rescue:+5V-power1 #PWR011
U 1 1 5862F5BC
P 6225 7125
F 0 "#PWR011" H 6225 6975 50  0001 C CNN
F 1 "+5V" H 6225 7265 50  0000 C CNN
F 2 "" H 6225 7125 50  0000 C CNN
F 3 "" H 6225 7125 50  0000 C CNN
	1    6225 7125
	1    0    0    -1  
$EndComp
Wire Wire Line
	2575 5500 2525 5500
Wire Wire Line
	6225 7175 6225 7125
Wire Wire Line
	2625 2250 2625 2800
Wire Wire Line
	2625 2800 2625 3350
Wire Wire Line
	2625 3350 2625 3850
Wire Wire Line
	2725 2900 2725 3450
Wire Wire Line
	2725 3450 2725 3950
Wire Wire Line
	2725 3950 2725 4250
Wire Wire Line
	9250 4000 9775 4000
Wire Wire Line
	9775 4100 9625 4100
Text Label 9625 4100 0    60   ~ 0
clk
Wire Wire Line
	9775 4750 9325 4750
Text Label 9325 4750 0    60   ~ 0
~SS
Wire Notes Line
	11200 3300 11200 3325
Wire Wire Line
	3075 3100 2525 3100
Wire Notes Line
	6950 3300 11200 3300
Wire Wire Line
	2750 1000 2775 1050
Wire Wire Line
	3525 1050 2775 1050
Connection ~ 2775 1050
Wire Wire Line
	2775 1050 2750 1100
Wire Wire Line
	2750 900  3525 900 
Wire Wire Line
	9775 2575 9425 2575
NoConn ~ 9775 5250
$Comp
L power:GND #PWR015
U 1 1 5BA09D35
P 9225 2475
F 0 "#PWR015" H 9225 2225 50  0001 C CNN
F 1 "GND" H 9230 2302 50  0000 C CNN
F 2 "" H 9225 2475 50  0001 C CNN
F 3 "" H 9225 2475 50  0001 C CNN
	1    9225 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	9225 2475 9775 2475
Wire Wire Line
	9775 4200 9550 4200
Wire Wire Line
	9775 4300 9550 4300
Text Label 9550 4300 0    60   ~ 0
MOSI
Text Label 9550 4200 0    60   ~ 0
MISO
Wire Wire Line
	7425 3850 7675 3850
Wire Wire Line
	8200 3850 8200 4200
Wire Wire Line
	8200 4200 8275 4200
Wire Wire Line
	7925 3850 7925 4000
Connection ~ 7925 3850
Wire Wire Line
	7925 3850 8200 3850
Wire Wire Line
	7675 4000 7675 3850
Connection ~ 7675 3850
Wire Wire Line
	7675 3850 7925 3850
Wire Wire Line
	7675 4300 7675 4400
Wire Wire Line
	7425 4400 7675 4400
Connection ~ 7675 4400
Wire Wire Line
	7675 4400 7925 4400
Wire Wire Line
	7925 4300 7925 4400
Connection ~ 7925 4400
Wire Wire Line
	7925 4400 8200 4400
$Comp
L shield-rescue:CONN_01X03-shield-rescue P10
U 1 1 5BA41D65
P 5825 6650
F 0 "P10" H 5750 6850 50  0000 C CNN
F 1 "Digi1" V 5925 6650 50  0000 C CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBA_2,5_3-G-5,08_1x03_P5.08mm_Horizontal" H 5825 6650 50  0001 C CNN
F 3 "" H 5825 6650 50  0000 C CNN
	1    5825 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6025 6750 6225 6750
Wire Wire Line
	6025 6650 6600 6650
Wire Wire Line
	6025 6550 6225 6550
Wire Wire Line
	6025 7175 6225 7175
Wire Wire Line
	6025 7275 6600 7275
Wire Wire Line
	6025 7375 6225 7375
Text Label 6600 7275 2    60   ~ 0
IO_MICRO_2
Wire Wire Line
	8275 4875 7425 4875
Wire Wire Line
	8275 4975 7425 4975
Wire Wire Line
	7425 4525 7425 4400
Wire Wire Line
	7425 3850 7425 3725
Wire Wire Line
	7425 3850 7175 3850
Wire Wire Line
	7175 3850 7175 3750
Connection ~ 7425 3850
Wire Wire Line
	7425 4400 7175 4400
Connection ~ 7425 4400
Wire Wire Line
	7175 4325 7175 4400
Wire Wire Line
	8625 1075 9275 1075
Wire Wire Line
	8625 1250 9275 1250
Wire Wire Line
	8625 1425 9275 1425
Wire Wire Line
	8625 1625 9275 1625
Text Label 9275 1075 2    60   ~ 0
clk
Text Label 9275 1250 2    60   ~ 0
MISO
Text Label 9275 1425 2    60   ~ 0
MOSI
Text Label 9275 1625 2    60   ~ 0
CS\SS
Wire Wire Line
	3000 2800 3000 3000
Wire Wire Line
	3400 4150 3400 3600
Wire Wire Line
	3075 2900 3525 2900
Wire Wire Line
	3525 2800 3000 2800
Wire Wire Line
	3325 3500 3525 3500
Wire Wire Line
	3325 3500 3325 4050
Wire Wire Line
	3400 3600 3525 3600
Wire Wire Line
	3525 3250 3250 3250
Wire Wire Line
	3250 3250 3250 3650
Wire Wire Line
	3525 3150 3150 3150
Wire Wire Line
	3150 3150 3150 3550
Wire Wire Line
	2525 2550 3525 2550
Wire Wire Line
	2525 2450 3525 2450
Wire Wire Line
	3075 3100 3075 2900
Text Notes 9475 1300 0    60   ~ 0
PWM1_CONN\n
Text Notes 6875 675  0    94   ~ 0
Digital analogique convecter\n
Text Label 7425 4875 0    60   ~ 0
IO_MICRO_1
Text Label 7425 4975 0    60   ~ 0
IO_MICRO_2
Wire Wire Line
	2975 1600 2575 1600
Wire Wire Line
	2575 1500 3525 1500
$Sheet
S 7225 800  1400 1250
U 5BB8AB57
F0 "F5_CAN" 60
F1 "F5_CAN.sch" 60
F2 "I1" I L 7225 1100 60 
F3 "I2" I L 7225 1250 60 
F4 "I3" I L 7225 1400 60 
F5 "I4" I L 7225 1575 60 
F6 "Udir" I L 7225 1725 60 
F7 "Udc" I L 7225 1875 60 
F8 "Uac" I L 7225 950 60 
F9 "clk" O R 8625 1075 60 
F10 "MISO" O R 8625 1250 60 
F11 "MOSI" I R 8625 1425 60 
F12 "CS_SS" B R 8625 1625 60 
$EndSheet
Text Label 6600 6650 2    60   ~ 0
IO_MICRO_1
NoConn ~ 8275 5075
NoConn ~ 8275 5175
NoConn ~ 8275 5275
NoConn ~ 8275 5375
NoConn ~ 8275 5475
NoConn ~ 8275 5575
$Comp
L shield-rescue:+5V-power1 #PWR09
U 1 1 5BA4222D
P 6225 6500
F 0 "#PWR09" H 6225 6350 50  0001 C CNN
F 1 "+5V" H 6225 6640 50  0000 C CNN
F 2 "" H 6225 6500 50  0000 C CNN
F 3 "" H 6225 6500 50  0000 C CNN
	1    6225 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6225 6550 6225 6500
$Comp
L shield-rescue:+5V-power1 #PWR02
U 1 1 5BA47324
P 2575 6000
F 0 "#PWR02" H 2575 5850 50  0001 C CNN
F 1 "+5V" H 2575 6140 50  0000 C CNN
F 2 "" H 2575 6000 50  0000 C CNN
F 3 "" H 2575 6000 50  0000 C CNN
	1    2575 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	2525 6000 2575 6000
$Comp
L shield-rescue:GND-power1 #PWR010
U 1 1 5BA5F4E6
P 6225 6750
F 0 "#PWR010" H 6225 6500 50  0001 C CNN
F 1 "GND" H 6225 6600 50  0000 C CNN
F 2 "" H 6225 6750 50  0000 C CNN
F 3 "" H 6225 6750 50  0000 C CNN
	1    6225 6750
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:GND-power1 #PWR012
U 1 1 5BA5F641
P 6225 7375
F 0 "#PWR012" H 6225 7125 50  0001 C CNN
F 1 "GND" H 6225 7225 50  0000 C CNN
F 2 "" H 6225 7375 50  0000 C CNN
F 3 "" H 6225 7375 50  0000 C CNN
	1    6225 7375
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 950  7225 950 
Wire Wire Line
	5875 2500 5875 1100
Wire Wire Line
	5875 1100 7225 1100
Wire Wire Line
	4925 2500 5875 2500
Wire Wire Line
	5975 1250 5975 2850
Wire Wire Line
	5975 1250 7225 1250
Wire Wire Line
	4925 2850 5975 2850
Wire Wire Line
	6075 3200 6075 1400
Wire Wire Line
	6075 1400 7225 1400
Wire Wire Line
	4925 3200 6075 3200
Wire Wire Line
	6175 1575 6175 3550
Wire Wire Line
	6175 1575 7225 1575
Wire Wire Line
	4925 3550 6175 3550
Wire Wire Line
	6575 4775 6575 1725
Wire Wire Line
	4925 4775 6575 4775
Wire Wire Line
	6575 1725 7225 1725
Wire Wire Line
	5400 1500 5400 1875
Wire Wire Line
	5400 1875 7225 1875
$EndSCHEMATC
